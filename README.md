# POC Simpletransformers
## Setup
```bash
conda create -n st python pandas tqdm
conda activate st
# gpu support
conda install pytorch>=1.6 cudatoolkit=11.0 -c pytorch
pip install simpletransformers
```

