from simpletransformers.classification import ClassificationModel, ClassificationArgs
import pandas as pd
import logging
import csv
import statistics

labels = ["colère", "peur", "tristesse", "joie"]
size = 5
switcher = {
    0: [1, 0, 0, 0, 0],
    1: [0, 1, 0, 0, 0],
    2: [0, 0, 1, 0, 0],
    3: [0, 0, 0, 1, 0],
    4: [0, 0, 0, 0, 1]
}

cr_arr = []
threshold = 0.4

# Change this to switch model
# model_index = "camembert"
model_index = "flaubert"


def int_to_emotion(emo_class):
     """
     """
     return switcher.get(emo_class)

logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

# Preparing train data
train_data = []
# Preparing eval data
eval_data = []

with open('data_train.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=';')
    for row in csv_reader:
        # todo skip 0s
        if(int(row["emotion"]) == 0):
            continue
        else:
            arr = []
            arr.append(row["txt"])
            arr.append(int(row["emotion"])-1)
            train_data.append(arr)
#print(train_data)

with open('data_test.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=';')
    for row in csv_reader:
        # todo skip 0s
        if(int(row["emotion"]) == 0):
            continue
        else:
            arr = []
            arr.append(row["txt"])
            arr.append(int(row["emotion"])-1)
            eval_data.append(arr)
#print(eval_data)

train_df = pd.DataFrame(train_data)
train_df.columns = ["text", "labels"]

eval_df = pd.DataFrame(eval_data)
eval_df.columns = ["text", "labels"]

# Optional model configuration
model_args = ClassificationArgs(num_train_epochs=1)

model_configs = {
    "flaubert" : ClassificationModel(
        'flaubert',
        'flaubert/flaubert_base_cased',
        use_cuda=True,
        num_labels=4,
        args=model_args),
    "camembert" : ClassificationModel(
        'camembert',
        'camembert-base',
        use_cuda=True,
        num_labels=4,
        args=model_args)
}

# Create a ClassificationModel
model = model_configs[model_index]

# Train the model
model.train_model(train_df)

# Evaluate the model
result, model_outputs, wrong_predictions = model.eval_model(eval_df)
print("Qualimétrie OOB")
print("results: ", result)
print("model_output: ", model_outputs)
print("wrong_predictions: ", wrong_predictions)

mean_error_by_classification = []
correct_answers = []

with open('data_test.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=';')
    for row in csv_reader:
        cr = {}

        txt = row["txt"]
        cr["txt"] = txt
        cr["expected"] = int_to_emotion(int(row["emotion"]))

        predictions, raw_outputs = model.predict(txt)
        cr["actual"] = raw_outputs[0]

        # todo: if expected != 0 -> normal
        # else if all prediction are under threshold add a correct answer 

        """if(int(row["emotion"]) == 0):
            print("todo")
            answer = 1
            for i in range(len(cr["actual"])):
                if (cr["actual"][i] > threshold):
                    answer = 0
            correct_answers.append(answer)


        else:"""
        if(int(row["emotion"]) == 0):
            continue    
        # error
        deltas = []
        for i in range(len(labels)):
            deltas.append(abs(cr["expected"][i] - cr["actual"][i]))

        mean_error_by_classification.append(statistics.mean(deltas))

        classification = [0, 0, 0, 0, 0]
        classification[predictions[0]+1] = 1
        """if (cr["actual"][predictions[0]] < threshold):
            correct_answers.append(0)
        else:"""
        correct_answers.append(1 if classification == cr["expected"] else 0)
        print(cr)

print("Qualimétrie Maison")
print(statistics.mean(mean_error_by_classification))
print(sum(correct_answers), " / ", len(correct_answers))